class AddAddressToClocks < ActiveRecord::Migration[5.2]
  def change
    add_column :clocks, :address, :string
  end
end
