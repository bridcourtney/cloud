class CreateLeaverequests < ActiveRecord::Migration[5.2]
  def change
    create_table :leaverequests do |t|
      t.date :fromdate
      t.date :todate
      t.integer :duration
      t.string :lmanageremail
      t.integer :lleavebalance
      t.integer :lleaveentitlement
      t.references :uprofile, foreign_key: true

      t.timestamps
    end
  end
end
