class CreateUprofiles < ActiveRecord::Migration[5.2]
  def change
    create_table :uprofiles do |t|
      t.string :firstname
      t.string :lastname
      t.string :manageremail
      t.integer :leavebalance
      t.integer :leaveentitlement
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
