class CreateClocks < ActiveRecord::Migration[5.2]
  def change
    create_table :clocks do |t|
      t.integer :hours
      t.string :geolocation_in
      t.string :geolocation_out
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
