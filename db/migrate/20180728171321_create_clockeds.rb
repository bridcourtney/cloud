class CreateClockeds < ActiveRecord::Migration[5.2]
  def change
    create_table :clockeds do |t|
      t.integer :hour
      t.string :location_in
      t.string :location_out
      t.date :crddate
      t.time :crdtime
      t.string :address
      t.string :latitude
      t.string :longitude
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
