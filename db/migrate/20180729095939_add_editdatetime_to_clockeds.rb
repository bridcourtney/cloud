class AddEditdatetimeToClockeds < ActiveRecord::Migration[5.2]
  def change
    add_column :clockeds, :eddate, :date
    add_column :clockeds, :edtime, :time
  end
end
