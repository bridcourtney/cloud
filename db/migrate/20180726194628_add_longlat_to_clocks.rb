class AddLonglatToClocks < ActiveRecord::Migration[5.2]
  def change
    add_column :clocks, :latitude, :float
    add_column :clocks, :longitude, :float
  end
end
