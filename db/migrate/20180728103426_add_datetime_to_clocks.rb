class AddDatetimeToClocks < ActiveRecord::Migration[5.2]
  def change
    add_column :clocks, :update, :date
    add_column :clocks, :uptime, :time
  end
end
