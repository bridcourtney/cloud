class AddStatusToLeaverequests < ActiveRecord::Migration[5.2]
  def change
    add_column :leaverequests, :status, :string
  end
end
