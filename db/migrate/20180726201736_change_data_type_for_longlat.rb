class ChangeDataTypeForLonglat < ActiveRecord::Migration[5.2]
  def self.up
    change_table :clocks do |t|
      t.change :longitude, :varchar
      t.change :latitude, :varchar
    end
  end
  def self.down
    change_table :clocks do |t|
      t.change :longitude, :varchar
      t.change :latitude, :varchar
    end
  end
end
