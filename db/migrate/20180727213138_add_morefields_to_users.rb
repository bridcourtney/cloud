class AddMorefieldsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :firstname, :string
    add_column :users, :lastname, :string
    add_column :users, :manageremail, :string
    add_column :users, :leavebalance, :integer
    add_column :users, :leaveentitlement, :integer
    add_column :users, :admin, :boolean
  end
end
