# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_31_105811) do

  create_table "clockeds", force: :cascade do |t|
    t.integer "hour"
    t.string "location_in"
    t.string "location_out"
    t.date "crddate"
    t.time "crdtime"
    t.string "address"
    t.string "latitude"
    t.string "longitude"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "eddate"
    t.time "edtime"
    t.datetime "updated"
    t.datetime "updatedt"
    t.time "edittimenw"
    t.index ["user_id"], name: "index_clockeds_on_user_id"
  end

  create_table "clocks", force: :cascade do |t|
    t.integer "hours"
    t.string "geolocation_in"
    t.string "geolocation_out"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "cdatetime"
    t.datetime "credte"
    t.datetime "crdt"
    t.string "latitude"
    t.string "longitude"
    t.string "address"
    t.date "update"
    t.time "uptime"
    t.index ["user_id"], name: "index_clocks_on_user_id"
  end

  create_table "leaverequests", force: :cascade do |t|
    t.date "fromdate"
    t.date "todate"
    t.integer "duration"
    t.string "lmanageremail"
    t.integer "lleavebalance"
    t.integer "lleaveentitlement"
    t.integer "uprofile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.integer "newbalance"
    t.date "eddate"
    t.time "edtime"
    t.index ["uprofile_id"], name: "index_leaverequests_on_uprofile_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "manageremail"
    t.integer "leavebalance"
    t.integer "leaveentitlement"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "uprofiles", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "manageremail"
    t.integer "leavebalance"
    t.integer "leaveentitlement"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_uprofiles_on_user_id"
  end

  create_table "userdetails", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "manageremail"
    t.integer "leavebalance"
    t.integer "leaveentitlement"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_userdetails_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.string "firstname"
    t.string "lastname"
    t.string "manageremail"
    t.integer "leavebalance"
    t.integer "leaveentitlement"
    t.boolean "admin"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
