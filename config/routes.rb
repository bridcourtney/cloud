Rails.application.routes.draw do
  resources :leaverequests
  get 'users/index'
  resources :clockeds
  resources :uprofiles
  root 'dashboard#index'
  resources :clocks
  devise_for :users
  match '/users',   to: 'users#index',   via: 'get'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
