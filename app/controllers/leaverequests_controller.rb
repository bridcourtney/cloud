  class LeaverequestsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_leaverequest, only: [:show, :edit, :update, :destroy]

  # GET /leaverequests
  # GET /leaverequests.json
  def index
    #@leaverequests = Leaverequest.all
    @leaverequests = Leaverequest.order("created_at desc")
    
 
  end

  # GET /leaverequests/1
  # GET /leaverequests/1.json
  def show
  end

  # GET /leaverequests/new
  def new
    @leaverequest = Leaverequest.new
    @leaverequest.uprofile_id = current_user.uprofile.id
    @leaverequest.lmanageremail = current_user.uprofile.manageremail
    @leaverequest.lleavebalance = current_user.uprofile.leavebalance
    @leaverequest.lleaveentitlement = current_user.uprofile.leaveentitlement
  end

  # GET /leaverequests/1/edit
  def edit
    @leaverequest.newbalance = (@leaverequest.lleavebalance - @leaverequest.duration)
  end

  # POST /leaverequests
  # POST /leaverequests.json
  def create
    @leaverequest = Leaverequest.new(leaverequest_params)
    @leaverequest.duration = (@leaverequest.todate - @leaverequest.fromdate) 
    respond_to do |format|
      if @leaverequest.save
        format.html { redirect_to @leaverequest, notice: 'Leaverequest was successfully created.' }
        format.json { render :show, status: :created, location: @leaverequest }
      else
        format.html { render :new }
        format.json { render json: @leaverequest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leaverequests/1
  # PATCH/PUT /leaverequests/1.json
  def update
    respond_to do |format|
      if @leaverequest.update(leaverequest_params)
        format.html { redirect_to @leaverequest, notice: 'Leaverequest was successfully updated.' }
        format.json { render :show, status: :ok, location: @leaverequest }
      else
        format.html { render :edit }
        format.json { render json: @leaverequest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leaverequests/1
  # DELETE /leaverequests/1.json
  def destroy
    @leaverequest.destroy
    respond_to do |format|
      format.html { redirect_to leaverequests_url, notice: 'Leaverequest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_leaverequest
      @leaverequest = Leaverequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def leaverequest_params
      params.require(:leaverequest).permit(:fromdate, :todate, :duration, :lmanageremail, :lleavebalance, :lleaveentitlement, 
      :status,  :uprofile_id)
    end
end
