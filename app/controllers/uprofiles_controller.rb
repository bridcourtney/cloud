class UprofilesController < ApplicationController
  before_action :set_uprofile, only: [:show, :edit, :update, :destroy]

  # GET /uprofiles
  # GET /uprofiles.json
  def index
    @uprofiles = Uprofile.all
  end

  # GET /uprofiles/1
  # GET /uprofiles/1.json
  def show
  end

  # GET /uprofiles/new
  def new
    @uprofile = Uprofile.new
  end
  
  # GET /uprofiles/1/edit
  def edit
  end

  # POST /uprofiles
  # POST /uprofiles.json
  def create
    @uprofile = Uprofile.new(uprofile_params)

    respond_to do |format|
      if @uprofile.save
        format.html { redirect_to @uprofile, notice: 'Uprofile was successfully created.' }
        format.json { render :show, status: :created, location: @uprofile }
      else
        format.html { render :new }
        format.json { render json: @uprofile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /uprofiles/1
  # PATCH/PUT /uprofiles/1.json
  def update
    respond_to do |format|
      if @uprofile.update(uprofile_params)
        format.html { redirect_to @uprofile, notice: 'Uprofile was successfully updated.' }
        format.json { render :show, status: :ok, location: @uprofile }
      else
        format.html { render :edit }
        format.json { render json: @uprofile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /uprofiles/1
  # DELETE /uprofiles/1.json
  def destroy
    @uprofile.destroy
    respond_to do |format|
      format.html { redirect_to uprofiles_url, notice: 'Uprofile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_uprofile
      @uprofile = Uprofile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def uprofile_params
      params.require(:uprofile).permit(:firstname, :lastname, :manageremail, :leavebalance, :leaveentitlement, :user_id)
    end
end
