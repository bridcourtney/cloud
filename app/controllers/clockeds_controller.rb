class ClockedsController < ApplicationController
  before_action :authenticate_user!, except: [:show]
  before_action :set_clocked, only: [:show, :edit, :update, :destroy]

  # GET /clockeds
  # GET /clockeds.json
  def index
    @clockeds = Clocked.order("created_at desc").where(user_id: current_user.clocked_ids + [current_user.id])
  end

  # GET /clockeds/1
  # GET /clockeds/1.json
  def show
  end

  # GET /clockeds/new
  def new
    @clocked = Clocked.new
    @clocked.user_id = current_user.id
    @clocked.location_in = current_user.current_sign_in_ip
    @clocked.crddate = DateTime.now
    @clocked.crdtime = DateTime.now
    @clocked.edittimenw = Time.now
    
  end

  # GET /clockeds/1/edit
  def edit
    @clocked.location_out = current_user.current_sign_in_ip
    @clocked.eddate = DateTime.now
    @clocked.edittimenw = Time.now
    @clocked.updatedt = DateTime.now
    @clocked.hour = (@clocked.updatedt - @clocked.created_at) / 1.hours
  end

  # POST /clockeds
  # POST /clockeds.json
  def create
    @clocked = Clocked.new(clocked_params)

    respond_to do |format|
      if @clocked.save
        format.html { redirect_to @clocked, notice: 'Clocked was successfully created.' }
        format.json { render :show, status: :created, location: @clocked }
      else
        format.html { render :new }
        format.json { render json: @clocked.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clockeds/1
  # PATCH/PUT /clockeds/1.json
  def update
    respond_to do |format|
      if @clocked.update(clocked_params)
        format.html { redirect_to @clocked, notice: 'Clocked was successfully updated.' }
        format.json { render :show, status: :ok, location: @clocked }
      else
        format.html { render :edit }
        format.json { render json: @clocked.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clockeds/1
  # DELETE /clockeds/1.json
  def destroy
    @clocked.destroy
    respond_to do |format|
      format.html { redirect_to clockeds_url, notice: 'Clocked was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_clocked
      @clocked = Clocked.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def clocked_params
      params.require(:clocked).permit(:hour, :location_in, :location_out, :crddate, :crdtime, :address, :latitude, :longitude, :user_id)
    end
end
