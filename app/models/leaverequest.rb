class Leaverequest < ApplicationRecord
  belongs_to :uprofile
  
  validate :new_balance_cannot_be_greater_than_leaveEntitlement_plus_oldBalance

 
  def new_balance_cannot_be_greater_than_leaveEntitlement_plus_oldBalance
    if duration > lleavebalance
      errors.add(:duration, "This would exceed you annual entitlement")
    end
  end
end
