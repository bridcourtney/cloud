class Clock < ApplicationRecord
  belongs_to :user
  
  geocoded_by :geolocation_in,
   :latitude => :latitude, :longitude => :longitude, :address => :address
 after_validation :geocode
 
 reverse_geocoded_by :latitude, :longitude,
   :address => :address
 after_validation :reverse_geocode
end
