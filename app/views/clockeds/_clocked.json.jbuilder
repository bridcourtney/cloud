json.extract! clocked, :id, :hour, :location_in, :location_out, :crddate, :crdtime, :address, :latitude, :longitude, :user_id, :created_at, :updated_at
json.url clocked_url(clocked, format: :json)
