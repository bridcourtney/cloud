json.extract! uprofile, :id, :firstname, :lastname, :manageremail, :leavebalance, :leaveentitlement, :user_id, :created_at, :updated_at
json.url uprofile_url(uprofile, format: :json)
