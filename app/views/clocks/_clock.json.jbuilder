json.extract! clock, :id, :hours, :geolocation_in, :geolocation_out, :user_id, :created_at, :updated_at
json.url clock_url(clock, format: :json)
