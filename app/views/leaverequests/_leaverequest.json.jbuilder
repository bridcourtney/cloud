json.extract! leaverequest, :id, :fromdate, :todate, :duration, :lmanageremail, :lleavebalance, :lleaveentitlement, :uprofile_id, :created_at, :updated_at
json.url leaverequest_url(leaverequest, format: :json)
