require 'test_helper'

class ClockedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @clocked = clockeds(:one)
  end

  test "should get index" do
    get clockeds_url
    assert_response :success
  end

  test "should get new" do
    get new_clocked_url
    assert_response :success
  end

  test "should create clocked" do
    assert_difference('Clocked.count') do
      post clockeds_url, params: { clocked: { address: @clocked.address, crddate: @clocked.crddate, crdtime: @clocked.crdtime, hour: @clocked.hour, latitude: @clocked.latitude, location_in: @clocked.location_in, location_out: @clocked.location_out, longitude: @clocked.longitude, user_id: @clocked.user_id } }
    end

    assert_redirected_to clocked_url(Clocked.last)
  end

  test "should show clocked" do
    get clocked_url(@clocked)
    assert_response :success
  end

  test "should get edit" do
    get edit_clocked_url(@clocked)
    assert_response :success
  end

  test "should update clocked" do
    patch clocked_url(@clocked), params: { clocked: { address: @clocked.address, crddate: @clocked.crddate, crdtime: @clocked.crdtime, hour: @clocked.hour, latitude: @clocked.latitude, location_in: @clocked.location_in, location_out: @clocked.location_out, longitude: @clocked.longitude, user_id: @clocked.user_id } }
    assert_redirected_to clocked_url(@clocked)
  end

  test "should destroy clocked" do
    assert_difference('Clocked.count', -1) do
      delete clocked_url(@clocked)
    end

    assert_redirected_to clockeds_url
  end
end
