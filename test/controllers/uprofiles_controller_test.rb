require 'test_helper'

class UprofilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @uprofile = uprofiles(:one)
  end

  test "should get index" do
    get uprofiles_url
    assert_response :success
  end

  test "should get new" do
    get new_uprofile_url
    assert_response :success
  end

  test "should create uprofile" do
    assert_difference('Uprofile.count') do
      post uprofiles_url, params: { uprofile: { firstname: @uprofile.firstname, lastname: @uprofile.lastname, leavebalance: @uprofile.leavebalance, leaveentitlement: @uprofile.leaveentitlement, manageremail: @uprofile.manageremail, user_id: @uprofile.user_id } }
    end

    assert_redirected_to uprofile_url(Uprofile.last)
  end

  test "should show uprofile" do
    get uprofile_url(@uprofile)
    assert_response :success
  end

  test "should get edit" do
    get edit_uprofile_url(@uprofile)
    assert_response :success
  end

  test "should update uprofile" do
    patch uprofile_url(@uprofile), params: { uprofile: { firstname: @uprofile.firstname, lastname: @uprofile.lastname, leavebalance: @uprofile.leavebalance, leaveentitlement: @uprofile.leaveentitlement, manageremail: @uprofile.manageremail, user_id: @uprofile.user_id } }
    assert_redirected_to uprofile_url(@uprofile)
  end

  test "should destroy uprofile" do
    assert_difference('Uprofile.count', -1) do
      delete uprofile_url(@uprofile)
    end

    assert_redirected_to uprofiles_url
  end
end
