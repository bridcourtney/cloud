require 'test_helper'

class LeaverequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @leaverequest = leaverequests(:one)
  end

  test "should get index" do
    get leaverequests_url
    assert_response :success
  end

  test "should get new" do
    get new_leaverequest_url
    assert_response :success
  end

  test "should create leaverequest" do
    assert_difference('Leaverequest.count') do
      post leaverequests_url, params: { leaverequest: { duration: @leaverequest.duration, fromdate: @leaverequest.fromdate, lleavebalance: @leaverequest.lleavebalance, lleaveentitlement: @leaverequest.lleaveentitlement, lmanageremail: @leaverequest.lmanageremail, todate: @leaverequest.todate, uprofile_id: @leaverequest.uprofile_id } }
    end

    assert_redirected_to leaverequest_url(Leaverequest.last)
  end

  test "should show leaverequest" do
    get leaverequest_url(@leaverequest)
    assert_response :success
  end

  test "should get edit" do
    get edit_leaverequest_url(@leaverequest)
    assert_response :success
  end

  test "should update leaverequest" do
    patch leaverequest_url(@leaverequest), params: { leaverequest: { duration: @leaverequest.duration, fromdate: @leaverequest.fromdate, lleavebalance: @leaverequest.lleavebalance, lleaveentitlement: @leaverequest.lleaveentitlement, lmanageremail: @leaverequest.lmanageremail, todate: @leaverequest.todate, uprofile_id: @leaverequest.uprofile_id } }
    assert_redirected_to leaverequest_url(@leaverequest)
  end

  test "should destroy leaverequest" do
    assert_difference('Leaverequest.count', -1) do
      delete leaverequest_url(@leaverequest)
    end

    assert_redirected_to leaverequests_url
  end
end
