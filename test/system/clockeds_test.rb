require "application_system_test_case"

class ClockedsTest < ApplicationSystemTestCase
  setup do
    @clocked = clockeds(:one)
  end

  test "visiting the index" do
    visit clockeds_url
    assert_selector "h1", text: "Clockeds"
  end

  test "creating a Clocked" do
    visit clockeds_url
    click_on "New Clocked"

    fill_in "Address", with: @clocked.address
    fill_in "Crddate", with: @clocked.crddate
    fill_in "Crdtime", with: @clocked.crdtime
    fill_in "Hour", with: @clocked.hour
    fill_in "Latitude", with: @clocked.latitude
    fill_in "Location In", with: @clocked.location_in
    fill_in "Location Out", with: @clocked.location_out
    fill_in "Longitude", with: @clocked.longitude
    fill_in "User", with: @clocked.user_id
    click_on "Create Clocked"

    assert_text "Clocked was successfully created"
    click_on "Back"
  end

  test "updating a Clocked" do
    visit clockeds_url
    click_on "Edit", match: :first

    fill_in "Address", with: @clocked.address
    fill_in "Crddate", with: @clocked.crddate
    fill_in "Crdtime", with: @clocked.crdtime
    fill_in "Hour", with: @clocked.hour
    fill_in "Latitude", with: @clocked.latitude
    fill_in "Location In", with: @clocked.location_in
    fill_in "Location Out", with: @clocked.location_out
    fill_in "Longitude", with: @clocked.longitude
    fill_in "User", with: @clocked.user_id
    click_on "Update Clocked"

    assert_text "Clocked was successfully updated"
    click_on "Back"
  end

  test "destroying a Clocked" do
    visit clockeds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Clocked was successfully destroyed"
  end
end
