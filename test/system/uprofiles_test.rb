require "application_system_test_case"

class UprofilesTest < ApplicationSystemTestCase
  setup do
    @uprofile = uprofiles(:one)
  end

  test "visiting the index" do
    visit uprofiles_url
    assert_selector "h1", text: "Uprofiles"
  end

  test "creating a Uprofile" do
    visit uprofiles_url
    click_on "New Uprofile"

    fill_in "Firstname", with: @uprofile.firstname
    fill_in "Lastname", with: @uprofile.lastname
    fill_in "Leavebalance", with: @uprofile.leavebalance
    fill_in "Leaveentitlement", with: @uprofile.leaveentitlement
    fill_in "Manageremail", with: @uprofile.manageremail
    fill_in "User", with: @uprofile.user_id
    click_on "Create Uprofile"

    assert_text "Uprofile was successfully created"
    click_on "Back"
  end

  test "updating a Uprofile" do
    visit uprofiles_url
    click_on "Edit", match: :first

    fill_in "Firstname", with: @uprofile.firstname
    fill_in "Lastname", with: @uprofile.lastname
    fill_in "Leavebalance", with: @uprofile.leavebalance
    fill_in "Leaveentitlement", with: @uprofile.leaveentitlement
    fill_in "Manageremail", with: @uprofile.manageremail
    fill_in "User", with: @uprofile.user_id
    click_on "Update Uprofile"

    assert_text "Uprofile was successfully updated"
    click_on "Back"
  end

  test "destroying a Uprofile" do
    visit uprofiles_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Uprofile was successfully destroyed"
  end
end
