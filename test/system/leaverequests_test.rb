require "application_system_test_case"

class LeaverequestsTest < ApplicationSystemTestCase
  setup do
    @leaverequest = leaverequests(:one)
  end

  test "visiting the index" do
    visit leaverequests_url
    assert_selector "h1", text: "Leaverequests"
  end

  test "creating a Leaverequest" do
    visit leaverequests_url
    click_on "New Leaverequest"

    fill_in "Duration", with: @leaverequest.duration
    fill_in "Fromdate", with: @leaverequest.fromdate
    fill_in "Lleavebalance", with: @leaverequest.lleavebalance
    fill_in "Lleaveentitlement", with: @leaverequest.lleaveentitlement
    fill_in "Lmanageremail", with: @leaverequest.lmanageremail
    fill_in "Todate", with: @leaverequest.todate
    fill_in "Uprofile", with: @leaverequest.uprofile_id
    click_on "Create Leaverequest"

    assert_text "Leaverequest was successfully created"
    click_on "Back"
  end

  test "updating a Leaverequest" do
    visit leaverequests_url
    click_on "Edit", match: :first

    fill_in "Duration", with: @leaverequest.duration
    fill_in "Fromdate", with: @leaverequest.fromdate
    fill_in "Lleavebalance", with: @leaverequest.lleavebalance
    fill_in "Lleaveentitlement", with: @leaverequest.lleaveentitlement
    fill_in "Lmanageremail", with: @leaverequest.lmanageremail
    fill_in "Todate", with: @leaverequest.todate
    fill_in "Uprofile", with: @leaverequest.uprofile_id
    click_on "Update Leaverequest"

    assert_text "Leaverequest was successfully updated"
    click_on "Back"
  end

  test "destroying a Leaverequest" do
    visit leaverequests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Leaverequest was successfully destroyed"
  end
end
